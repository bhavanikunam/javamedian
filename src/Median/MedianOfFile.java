package Median;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

public class MedianOfFile {

	static int M = 1000000; // max items the memory buffer can hold

	public static void externalSort(String fileName) {
		String tfile = "temp-file-";
		ArrayList<Integer> list = new ArrayList<>(M);
		int totalcount = 0;
		try {
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			int i, j;
			i = j = 0;
			boolean flag = true;
			// Iterate through the elements in the file
			while (flag) {
				// Read M-element chunk at a time from the file
				for (j = 0; j < M; j++) {

					String t = br.readLine();
					if (t != null) {

						list.add(Integer.parseInt(t));
						totalcount++;
					} else {
						flag = false;

						break;
					}
				}
				// Sort M elements
				Collections.sort(list);
				// Write the sorted numbers to temp file
				FileWriter fw = new FileWriter(tfile + Integer.toString(i) + ".txt");
				PrintWriter pw = new PrintWriter(fw);
				for (int k : list) {

					pw.println(k);
				}
				pw.close();
				i++;
				list.clear();
			}
			br.close();
			int slices = i;
			// Now open each file and merge them, then write back to disk
			int[] topNums = new int[slices];
			BufferedReader[] brs = new BufferedReader[slices];
			for (i = 0; i < slices; i++) {
				brs[i] = new BufferedReader(new FileReader(tfile + Integer.toString(i) + ".txt"));
				String t = brs[i].readLine();
				if (t != null)
					topNums[i] = Integer.parseInt(t);
				else
					topNums[i] = Integer.MAX_VALUE;
			}
			FileWriter fw = new FileWriter("external-sorted.txt");
			PrintWriter pw = new PrintWriter(fw);
			for (i = 0; i < totalcount; i++) {
				int min = topNums[0];
				int minFile = 0;
				for (j = 0; j < slices; j++) {
					if (min > topNums[j]) {
						min = topNums[j];
						minFile = j;
					}
				}
				pw.println(min);
				String t = brs[minFile].readLine();
				if (t != null)
					topNums[minFile] = Integer.parseInt(t);
				else
					topNums[minFile] = Integer.MAX_VALUE;
			}
			for (i = 0; i < slices; i++)
				brs[i].close();
			for (i = 0; i < slices; i++) {
				Files.delete(Paths.get(tfile + Integer.toString(i) + ".txt"));
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		getMedian(totalcount);

	}

	public static void getMedian(int count) {
		int lin = 0;
		if (count % 2 == 1) {
			lin = count / 2;
			String line;
			try {
				line = Files.lines(Paths.get("external-sorted.txt")).skip(lin).findFirst().get();
				System.out.print(line);
				Files.delete(Paths.get("external-sorted.txt"));
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {

			lin = count / 2;
			String line, line2;
			try {

				line = Files.lines(Paths.get("external-sorted.txt")).skip(lin).findFirst().get();
				line2 = Files.lines(Paths.get("external-sorted.txt")).skip(lin - 1).findFirst().get();
				double t = (double) (Integer.parseInt(line) + Integer.parseInt(line2)) / 2;
				System.out.print(t);
				Files.delete(Paths.get("external-sorted.txt"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) {
		String fileName = args[0];
		externalSort(fileName);
	}
}
